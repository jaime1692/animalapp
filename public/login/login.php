<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- posting database values -->
<?php
    if(isset($_POST['login'])) {
        $chkLogin = false;
        // User table attributes
        $uName = isset($_POST['uname'])? $_POST['uname'] : "";
        $uPassword = isset($_POST['psw'])? $_POST['psw'] : "";
        $encry_pass = md5($uPassword);
        // query
        openDB();

        $query =
          "
          SELECT
            uid,
            uusername,
            upassword
          FROM
            user
          WHERE
            uusername = '$uName'
          AND
            upassword = '$encry_pass'
          LIMIT 1
        ";

        $result = $db->query($query);
        $row = $result->fetch_assoc();
        if($row['uusername'] == $uName) {
          if($row['upassword'] == $encry_pass) {
            $_SESSION['authUser'] = $row['uid'];
            header("Location: ../map/map.php");
          }
        }
    
        if($chkLogin == false) {
          $msg = "Login failed! Please try again.";
        }

        echo '<script type="text/javascript"> var msg = "'.$msg.'";</script>';

        closeDB();
    }
?>
<!-- View -->
<svg class="login_path_6" viewBox="320 -12.303 706.082 517.852">
    <path fill="rgba(208,229,122,0.596)" id="Path_6" d="M 344.3957214355469 1.408484697341919 C 534.96337890625 -81.49501037597656 688.7914428710938 238.6431579589844 688.7914428710938 238.6431579589844 C 688.7914428710938 238.6431579589844 447.6611022949219 399.8699951171875 344.3957214355469 475.8778381347656 C 241.1303253173828 551.8856811523438 -96.99839782714844 485.0256042480469 0 238.6431579589844 C 96.99839782714844 -7.739273071289062 153.8280792236328 84.31198120117188 344.3957214355469 1.408484697341919 Z">
    </path>
</svg>
<svg class="login_path_3" viewBox="320 -18.052 706.082 523.6">
    <path fill="rgba(204,229,122,0.596)" id="Path_3" d="M 344.3957214355469 1.408484697341919 C 534.96337890625 -81.49501037597656 688.7914428710938 120.0258178710938 688.7914428710938 238.6431579589844 C 688.7914428710938 296.1722412109375 632.0722045898438 335.8226013183594 561.813232421875 370.1033325195312 C 487.3252258300781 406.447509765625 397.5776672363281 436.7335815429688 344.3957214355469 475.8778381347656 C 241.1303253173828 551.8856811523438 -96.99839782714844 485.0256042480469 0 238.6431579589844 C 96.99839782714844 -7.739273071289062 153.8280792236328 84.31198120117188 344.3957214355469 1.408484697341919 Z">
    </path>
</svg>
<svg class="login_path_2" viewBox="320 -18.052 706.082 523.6">
    <linearGradient spreadMethod="pad" id="LinearGradientFill1" x1="0.299" x2="0.918" y1="0.353" y2="0.867">
        <stop offset="0" stop-color="#a0e57a" stop-opacity="1"></stop>
        <stop offset="1" stop-color="#7ae1e5" stop-opacity="1"></stop>
    </linearGradient>
    <path fill="url(#LinearGradientFill1)" stroke="rgba(160,229,122,1)" stroke-width="1px" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="4" shape-rendering="auto" id="Path_2" d="M 344.3957214355469 1.408484697341919 C 534.96337890625 -81.49501037597656 688.7914428710938 120.0258178710938 688.7914428710938 238.6431579589844 C 688.7914428710938 357.260498046875 447.6611022949219 399.8699951171875 344.3957214355469 475.8778381347656 C 241.1303253173828 551.8856811523438 -96.99839782714844 485.0256042480469 0 238.6431579589844 C 96.99839782714844 -7.739273071289062 153.8280792236328 84.31198120117188 344.3957214355469 1.408484697341919 Z">
    </path>
</svg>
<div class="login-content">
    <img class="login_user_img" src="../../uploads/images/user.png" alt="Smiley face" height="112" width="112">
    <form method="post" action="<?PHP echo $_SERVER['PHP_SELF']; ?>">
        <input class="login_username" type="text" placeholder="Username" id="uname" name="uname" required>
        <input class="login_password" type="password" placeholder="Enter Password" id="psw" name="psw" required>
        <input class="hidden" type="checkbox" id="login" name="login" value="login" checked>
        <button class="login_button" type="submit" value="Submit" >Login</button>
    </form>
    <a class="register_button" href="../register/register.php">Register</a>
</div>
<!-- toast -->
<?php include_once '../includes/toast.html'; ?>
<!-- View controller -->
<script type="text/javascript" src="loginCtrl.js"></script>