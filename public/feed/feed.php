<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- Navegation bar -->
<?php include_once '../includes/feedNav.php'; ?>
<!-- posting database values -->
<?php

    $msg = "";
    $uid = isset($_SESSION['authUser'])? $_SESSION['authUser'] :"";
    openDB();
    $querySelect =
    "
    SELECT 
        `feedid`,
        `userid`,
        `uname`,
        `text`, 
        `image`, 
        `likes` 
    FROM 
        `feed`
    INNER JOIN
        `user`
    ON
        `uid` = `userid`
    WHERE
        userid IN (
            SELECT 
                user.uid
            FROM 
                `user`
            WHERE
                user.uid <> $uid
            AND 
            (
                user.uid IN 
                (
                    SELECT friendid
                FROM 
                    friends
                where
                    friends.uid = $uid
                and 
                    status = 'accepted'
                )
                OR
                user.uid IN 
                (
                    SELECT friends.uid 
                FROM 
                    friends
                where
                    friendid = $uid
                and 
                    status = 'accepted'
                )
            )
        )
        OR
        userid = $uid
    ";

    $resultSelect = $db->query($querySelect);

    if(isset($_POST['publish'])) {
        $comment = isset($_POST['comment'])? $_POST['comment'] :"";
        $target_dir = "../../uploads/images/";
        $filename = explode(".",basename($_FILES["fileToUpload"]["name"]));
        $encrypfilename = md5($filename[0]).".".$filename[1];
        $target_file = $target_dir . $encrypfilename;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $queryInsert =
        "
        INSERT INTO 
            `feed`
            (
                `userid`, 
                `text`, 
                `image`
            ) 
        VALUES 
            (
                $uid,
                '$comment',
                '".$target_file."'
            )
        ";
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            $msg = "Sorry, only JPG, JPEG and PNG files are allowed.";
            $uploadOk = 0;
        }
        
        if ($uploadOk == 0) {
            $msg = "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $msg = "The file has been uploaded.";
                $result = $db->query($queryInsert);
            } else {
                $msg = "Sorry, there was an error uploading your file.";
            }
        }
    }
    echo '<script type="text/javascript"> var msg = "'.$msg.'";</script>';
    closeDB();
?>
<!-- View -->
<div class="feed-panel">
    <div class="social-head-button">
        <button id="feed">Social Feed</button>
        <button id="add">Post</button>
    </div>
    <div id="social-panel">
    <h3 class="social-title-list"> My feed </h3>
        <?php
            while ($row = $resultSelect->fetch_assoc()) {
                $queryLikes = "SELECT count(*)  as total FROM  comments WHERE feedid = ".$row['feedid'];
                $resultLikes = $db->query($queryLikes);
                $likes = $resultLikes->fetch_assoc();
                $queryComments = "SELECT count(*)  as total FROM  comments WHERE feedid = ".$row['feedid'];
                $resultcomments = $db->query($queryComments);
                $comments = $resultcomments->fetch_assoc();
                echo '<div class="feed-entry" style="background:url('.$row['image'].');background-size: cover;" id="'.$row['feedid'].'">';
                echo '<p class="title">'.$row['text'].'</p>';
                echo '<div class="social">';
                echo '<p id="name">'.$row['uname'].'</p>';
                echo '<p id="likes">'.$likes['total'].' <i class="fas fa-heart"></i></p>';
                echo '<p id="comments">'.$comments['total'].' <i class="fas fa-comments"></i></p>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>
    <div id="post-feed-panel">
        <h3 class="social-title-list"> Post an entry </h3>
        <form class="posting-form" method="post" action="<?PHP echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
            <h6>Share a picture</h6>
            <div class="custom-file">            
                <input type="file" class="custom-file-input" name="fileToUpload" id="fileToUpload">
                <label class="custom-file-label" for="fileToUpload">Choose file...</label>
            </div>
            <h6>Comment</h6>
            <textarea id="comment" name="comment" rows="4" cols="30">
            </textarea>
            <input class="hidden" type="checkbox" id="publish" name="publish" value="publish" checked>
            <button class="settings submit" type="submit">publish</button>
        </form>
    </div>
</div>
<!-- toast -->
<?php include_once '../includes/toast.html'; ?>
<!-- View controller -->
<script type="text/javascript" src="feedCtrl.js"></script>
<!-- Footer -->
<?php include_once '../includes/footer.php'; ?>