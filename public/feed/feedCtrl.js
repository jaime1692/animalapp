// Erase background image
document.body.style.backgroundImage = "none";
document.getElementById("body").style.backgroundImage = "none";
$('#nav-title').html('My feed');

$(back).click(function(){
    window.location.href = "../map/map.php";
});

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    console.log(fileName);
});

if(msg) {
    $("#toast").toast({ delay: 1500 });
    $("#toast").toast("show");
    $(".toast-body").html(msg);
}
$("#social-panel").show();
$("#post-feed-panel").hide();
$("#feed").css("background","#a0e679");
$("#feed").css("border","2px solid #66944d");
$("#feed").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
$("#feed").css("color","#155724");

$(".close").click(function(){
    location.reload();
});

$("#feed").click(function(){
    $("#social-panel").show();
    $("#post-feed-panel").hide();
    $("#feed").css("background","#a0e679");
    $("#feed").css("border","2px solid #66944d");
    $("#feed").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
    $("#feed").css("color","#155724");
    $("#add").css("background","#e9ecef");
    $("#add").css("border","1px solid #c3c3c3");
    $("#add").css("box-shadow","none");
    $("#add").css("color","#8e8e8e");
});

$("#add").click(function(){
    $("#social-panel").hide();
    $("#post-feed-panel").show();
    $("#feed").css("background","#e9ecef");
    $("#feed").css("border","1px solid #c3c3c3");
    $("#feed").css("box-shadow","none");
    $("#feed").css("color","#8e8e8e");
    $("#add").css("background","#a0e679");
    $("#add").css("border","2px solid #66944d");
    $("#add").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
    $("#add").css("color","#155724");
});