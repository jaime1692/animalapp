        <svg class="smallfooter_path_15" viewBox="260 0 515 455">
            <linearGradient spreadMethod="pad" id="LinearGradientFill1" x1="0.182" x2="0.731" y1="-0.027" y2="0.527">
                <stop offset="0" stop-color="#a0e57a" stop-opacity="1"></stop>
                <stop offset="1" stop-color="#e5e57a" stop-opacity="1"></stop>
            </linearGradient>
            <path fill="url(#LinearGradientFill1)" id="Path_15" d="M 257.5 0 C 399.7133178710938 0 515 101.855224609375 515 227.5 C 515 353.144775390625 399.7133178710938 455 257.5 455 C 115.2866821289062 455 0 353.144775390625 0 227.5 C 0 101.855224609375 115.2866821289062 0 257.5 0 Z">
            </path>
        </svg>
        <footer>
        </footer>
        <!-- Include Javascript -->
        <?php include_once '../../server/includes/javascript.html'; ?>
    <div>
    </body>
</html>
