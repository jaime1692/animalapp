<nav class="navbar">
    <button class="control-panel back" id="back"><i class="fas fa-chevron-left"></i></button>
    <button class="control-panel config" onclick="location.href='../settings/settings.php';"><i class="fas fa-cog"></i></button>
    <img class="nav_user_img" src="../../uploads/images/user.png" alt="Smiley face" height="80" width="80">
    <h1>Welcome!</h1>
    <button  onclick="location.href='../feed/feed.php';">My feed</button>
</nav>
<svg class="nav_Ellipse_6" viewBox="240 0 515 455">
    <ellipse fill="rgba(160,229,122,1)" id="Ellipse_6" rx="236" ry="235.5" cx="236" cy="235.5">
    </ellipse>
</svg>
<svg class="nav_Path_8" viewBox="260 0 515 455">
    <linearGradient spreadMethod="pad" id="LinearGradientFill2" x1="0.5" x2="0.5" y1="0.138" y2="1">
        <stop offset="0" stop-color="#7ae1e5" stop-opacity="1"></stop>
        <stop offset="1" stop-color="#a0e57a" stop-opacity="1"></stop>
    </linearGradient>
    <path fill="url(#LinearGradientFill2)" stroke="rgba(160,229,122,1)" stroke-width="1px" stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="4" shape-rendering="auto" id="Path_8" d="M 257.5 0 C 399.7133178710938 0 515 101.855224609375 515 227.5 C 515 353.144775390625 399.7133178710938 455 257.5 455 C 115.2866821289062 455 0 353.144775390625 0 227.5 C 0 101.855224609375 115.2866821289062 0 257.5 0 Z">
    </path>
</svg>

