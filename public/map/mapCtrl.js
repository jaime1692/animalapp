$(back).click(function(){
    window.location.href = "../login/login.php";
});
$(".mapboxgl-popup-content").click(function(){
    window.location.href = "../test/test.php";
});

/* user's position */
var truckLocation = [153.015225,-27.499484];
var userPosition = {latitude: -27.499484, longitude: 153.015225};
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        userPosition = {"latitude":latitude,"longitude":longitude};
    });
}

/* Mapping */
var actual_JSON;
var center = {lat: -27.499484, lng: 153.015225};

mapboxgl.accessToken = 'pk.eyJ1IjoiamFpbWUxNjkyIiwiYSI6ImNrMG01NjRtNDEzODYzbm12ZWd4Zm82NHgifQ.4bbxwtQ5W7WIRyE6oZHJ7A';
var map = new mapboxgl.Map({
container: 'map', // container id
style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
center: center, // starting position at UQ [lng, lat] [153.013286,-27.497515]
zoom: 14 // starting zoom
});

var urlSpecies = "https://apps.des.qld.gov.au/species/?op=getspecies&kingdom=animals&family=myobatrachidae";
var url = "//apps.des.qld.gov.au/species/?op=getsurveysbyspecies&taxonid=568";
//url = "../../uploads/json/surveys.json";
url = "../../uploads/json/getSpecies.json";
function loadJSON(url,callback) {   
    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);  
}

var totalSpecies = [];
    
loadJSON(url,function(response) {
    // Parse JSON string into object
    actual_JSON = JSON.parse(response);
    var i;
    // Constructing new JSON to bind to the map
    for (i = 0; i < actual_JSON.features.length; i++) {
        var lng = actual_JSON.features[i].geometry.coordinates[0];
        var lat = actual_JSON.features[i].geometry.coordinates[1];
        var id = actual_JSON.features[i].properties.TaxonID
        var speciesLocation = { "type" : "Feature",
                                 "geometry" : { "type" : "Point" , "coordinates" : [lng,lat] }, 
                                 "properties" : { "title": id, "icon" : "veterinary" }
                              };
        totalSpecies.push(speciesLocation);
    }
});

var directions = [];
map.on('load', function () {
    // Add geolocate control to the map.
    new mapboxgl.GeolocateControl({
        positionOptions: {
        enableHighAccuracy: true
        },
        trackUserLocation: true
    });

    /*
    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
        "type": "geojson",
        "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": [truckLocation,[153.013458, -27.498438],[153.015213, -27.497629]]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 8
        }
    });
    */
    //creating user position marker
    var marker = document.createElement('div');
    marker.classList = 'truck';
  
    // Create a new marker
    truckMarker = new mapboxgl.Marker(marker)
      .setLngLat(truckLocation)
      .addTo(map);
    
    // Creating the marketrs for animals
    map.addLayer({
        "id": "symbols",
        "type": "symbol",
        "source": {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": totalSpecies
            }
        },
        "layout": {
            "icon-image": "{icon}-15",
            "text-field": "{title}",
            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
            "text-offset": [0, 0.6],
            "text-anchor": "top"
        }
    });
});

var specieInfo;
var specieImage;
map.on('click', 'symbols', function (e) {
    // Ajax call to capture an animal
    $.ajax({
        url: "../services/getInfo.php",
        type: "post",
        data: { 
            id : e.features[0].properties.title, 
        },
        cache: true,
        success: function(response){     
                console.log("Animal information");
                console.log(response);
                specieInfo = response.Species;  
                console.log(specieInfo.ScientificName);       
        }  
    });

    if(specieInfo.Image){
        specieImage = specieInfo.Image[0].URL+"."+specieInfo.Image[0].Format;
    }
    // Relocation on click
    map.flyTo({center: e.features[0].geometry.coordinates});

    // Adding the popup on the target
    var popup = new mapboxgl.Popup({ offset: 25 })
    .setText(+"<button>Catch</button>");
    var h6 = "<h6>"+specieInfo.FamilyCommonName+"</h6>";
    var button = '<button class="specie-btn" onclick="redirect('+"'"+e.features[0].properties.title+"','"+specieInfo.ScientificName+"','"+specieInfo.FamilyCommonName+"','"+specieImage+"','"+e.features[0].geometry.coordinates+"'"+')">Catch</button>';
    popup.setHTML(h6+button);
    
    var el = document.createElement('div');
    el.id = 'marker';
    el.className =  'animal_'+e.features[0].properties.title;
    if(specieImage){
        el.style.backgroundImage = "url('"+specieImage+"')";
    }
    console.log(specieImage);
    new mapboxgl.Marker(el)
    .setLngLat(e.features[0].geometry.coordinates)
    .setPopup(popup)
    .addTo(map);
});
     
// Change the cursor to a pointer when the it enters a feature in the 'symbols' layer.
map.on('mouseenter', 'symbols', function () {
    map.getCanvas().style.cursor = 'pointer';
});
    
// Change it back to a pointer when it leaves.
map.on('mouseleave', 'symbols', function () {
    map.getCanvas().style.cursor = '';
});

// redirect to catch animal
function redirect(id,name,cname,img,coor){
    console.log("redirect to "+id);
    window.location.href = "../catch/catch.php?id="+id+"&name="+name+"&cname="+cname+"&img="+img+"&coor="+coor;
}