<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- Navegation bar -->
<?php include_once '../includes/navbar.php'; ?>
<!-- View -->
<div id='map'></div>
<div class="recomm_panel" id='recommendation'>
    <span id="close">X</span>
    <div id="entries">
    </div>  
</div>
<!-- Footer -->
<?php include_once '../includes/footerMap.php'; ?>
<!-- Map controller -->
<script type="text/javascript" src="mapCtrl.js"></script>
<!-- Search controller -->
<script type="text/javascript" src="placesCtrl.js"></script>