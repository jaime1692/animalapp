<?php
    header('Content-Type: application/json');

    $specieID = isset($_POST['id'])? $_POST['id'] :"";
    $url = "https://apps.des.qld.gov.au/species/?op=getspeciesbyid&taxonid=" . $specieID;
	$data = file_get_contents($url);
    
    echo $data;
?>