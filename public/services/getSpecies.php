<?php

    header('Content-Type: application/json');
	$data = file_get_contents("https://apps.des.qld.gov.au/species/?op=getfamilynames&kingdom=animals&class=Mammalia");
    $dataJson = json_decode($data);

    foreach ($dataJson->Family as $arr1) {
        $ulrSpecies = $arr1->SpeciesUrl;
        $dataSpecies = file_get_contents($ulrSpecies);
        $dataSpeciesJson = json_decode($dataSpecies);
        $j = 0;
        foreach ($dataSpeciesJson->Species as $arr2) {
            ++$j;
            $id   = $arr2->TaxonID;
            $ulr = "https://apps.des.qld.gov.au/species/?op=getsurveysbyspecies&taxonid=".$id;
            $dataSurveys = file_get_contents($ulr);
            $dataSurveysJson = json_decode($dataSurveys);
            if($j == 20){
                break;
            }
            $i = 0;
            foreach ($dataSurveysJson->features as $arr3) {
                ++$i;
                echo json_encode($arr3);
                if($i == 100){
                    break;
                }
            }
        }
    }    
    //print_r($dataJson);
?>