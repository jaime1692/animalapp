<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- Navegation bar -->
<?php include_once '../includes/feedNav.php'; ?>
<!-- posting database values -->
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

    $uid = isset($_SESSION['authUser'])? $uid = $_SESSION['authUser'] :"";
    openDB();
    $querySelect =
    "
      SELECT
        `aid`, 
        `uid`, 
        `specieid`,
        `name`,
        `path`
      FROM
        album
      WHERE
        uid = '$uid'
    ";
    $resultSelect = $db->query($querySelect);

    $querySelectTotal =
    "
      SELECT
        count(*) as total
      FROM
        album
      WHERE
        uid = '$uid'
    ";
    $resultSelectTotal = $db->query($querySelectTotal);
    $total = $resultSelectTotal->fetch_assoc();
    closeDB();
?>
<!-- View -->
<div class="feed-panel">
    <?php
        echo '<h3 class="album-title-list">'.$total['total'].' animals collected</h3>';

        while ($row = $resultSelect->fetch_assoc()) {
          echo '<div class="row" align="center">';
          for ($x = 0; $x < 4; $x++) {
            if(!$row){
              break;
            }
            if($x != 0){
              $row = $resultSelect->fetch_assoc();
              if(!$row){
                break;
              }
              echo '<div class="album-entry" style="background:url('.$row['path'].');background-size: cover;" onclick="redirect('."'".$row['specieid']."','".$row['name']."','".$row['path']."'".')"></div>';
            } else {
              echo '<div class="album-entry" style="margin-left: 35px;background:url('.$row['path'].');background-size: cover;" onclick="redirect('."'".$row['specieid']."','".$row['name']."','".$row['path']."'".')"></div>';
            }
          }
          echo '</div>';
        }
    ?>
</div>
<!-- View controller -->
<script type="text/javascript" src="albumCtrl.js"></script>
<!-- Footer -->
<?php include_once '../includes/footer.php'; ?>