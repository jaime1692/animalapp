<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- Navegation bar -->
<?php include_once '../includes/feedNav.php'; ?>
<!-- posting database values -->
<script>
    // Erase background image
    document.body.style.backgroundImage = "none";
    document.getElementById("body").style.backgroundImage = "none";

    $(back).click(function(){
        window.location.href = "../album/album.php";
    });


    var url_string = window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    var name = url.searchParams.get("name");

    $('#nav-title').text(name);
    $('#nav-title').css('font-size',"27px");
    $('#nav-title').css('left',"18%");

    var specieInfo;
    $.ajax({
        url: "../services/getInfo.php",
        type: "post",
        data: { 
            id : id, 
        },
        cache: true,
        success: function(response){     
            console.log("Animal information");
            specieInfo = response.Species;  
            console.log(specieInfo);  
            $('.info_animal').append("<strong>Family common name</strong><p>"+specieInfo.FamilyCommonName+"</p>")  
            $('.info_animal').append("<strong>Class common name</strong><p>"+specieInfo.ClassCommonName+"</p>")  
            $('.info_animal').append("<strong>Conservation status</strong><p>"+specieInfo.ConservationStatus.NCAStatus+"</p>")            
            $('.info_animal').append("<strong>Family name</strong><p>"+specieInfo.FamilyName+"</p>")  
            $('.info_animal').append("<strong>Scientific name</strong><p>"+specieInfo.ScientificName+"</p>")  
            $('.info_animal').append("<strong>Taxonomy author</strong><p>"+specieInfo.TaxonomyAuthor+"</p>")  
        }  
    });
    
</script>
<!-- View -->
<div class="feed-panel">
    <?php
        echo '<div class="animal_photo-view" style="background:url('.$_GET['img'].');"></div>';
    ?>
    <div class="info_animal">
    </div>
</div>
<style>
    .animal_photo-view{
        width: 350px;
        height: 192px;
        background: #6c757d;
        margin: 12px;
        margin-bottom: 31px;
        border-radius: 15px;
        box-shadow: -1px 5px 11px 0px rgba(158,158,158,1);
        background-size: cover !important;
        margin-top: 137px;
    }
    .info_animal{
        padding: 10px 20px;
        margin-bottom: 50px;
        text-align: center;       
    }
    .info_animal strong{
        color: #15732b;
        font-weight: bolder;
        font-size: 18px;      
    }
    .info_animal p{
        text-transform: capitalize;
    }
</style>
<!-- View controller -->
<script type="text/javascript" src="animalCtrl.js"></script>
<!-- Footer -->
<?php include_once '../includes/footer.php'; ?>