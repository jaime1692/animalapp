<?php
    include_once '../config/server.php';

    $uid = isset($_SESSION['authUser'])? $_SESSION['authUser'] :"";

    // query
    openDB();
    if (isset($_POST['add'])) {
        $add = isset($_POST['add'])? $_POST['add'] :"";
        $date = date('Y-m-d h:m:s', time());
        $queryInsert = "
            INSERT INTO `friends`
                (`uid`, 
                `friendid`, 
                `time`, 
                `status`) 
            VALUES 
                (
                $uid,
                $add,
                '$date',
                'request')
        ";
        $result = $db->query($queryInsert);
    }

    if (isset($_POST['remove'])) {
        $rm = isset($_POST['remove'])? $_POST['remove'] :"";
        $queryDelete = "
            DELETE FROM 
                `friends`
            WHERE 
            (
                uid = $uid
            AND
                friendid = $rm
            )
            OR
            (
                uid = $rm
            AND
                friendid = $uid                
            )
        ";
        $result = $db->query($queryDelete);
    }

    if (isset($_POST['accept'])) {
        $rq = isset($_POST['accept'])? $_POST['accept'] :"";
        $queryDelete = "
            UPDATE
                `friends`
            SET
                `status` = 'accepted'
            WHERE 
                uid = $rq
            AND
                friendid = $uid
        ";
        $result = $db->query($queryDelete);
    }
    closeDB();
    echo $msg;
?>